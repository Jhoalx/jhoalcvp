<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

trait Uuids
{
	/**
	 * Boot the Uuid trait for the model.
	 *
	 * @return void
	 */
	public static function boot()
	{
		static::creating(function (Model $model) {
			$model->incrementing = false;
			$model->{$model->getKeyName()} = (string)Uuid::uuid4();
		});
	}
}