<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::get('/', 'FrontController@index')->name('home');
Route::get('blog', 'FrontController@blog')->name('blog');
Route::get('contacto', 'FrontController@contacto')->name('contact');

//Store Contact Request
Route::post('contacto', 'ContactController@store');

//Ruta de prueba para el mailable de contacto
Route::get('mailable', function () {
	$contact = App\Contact::first();
	return new App\Mail\ContactStored($contact);
});


Route::group(['middleware' => ['web']], function () {
	Route::get('lang/{lang}', function ($lang) {
		session(['lang' => $lang]);
		return Redirect::back();
	})->where(['lang' => 'en|es']);
});

