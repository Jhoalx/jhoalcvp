<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\ContactStored;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;


class ContactController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage and send mail to user
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Support\Facades\Redirect
	 *
	 */
	public function store(ContactRequest $request)
	{

		//store in database
		$contact = Contact::create($request->all());

		Mail::to($request->input('email'))
			->send(new ContactStored($contact));

		//redirect to clean contact view and open "success" modal
		return Redirect('contacto')->with(['status' => 'ContactStored']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Contact $contact
	 * @return \Illuminate\Http\Response
	 */
	public function show(Contact $contact)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Contact $contact
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Contact $contact)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\Contact $contact
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Contact $contact)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Contact $contact
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Contact $contact)
	{
		//
	}
}
