<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Contact Page Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used within Contact Form Viewq
	|
	*/

	'form-sectionTitle' => 'Contact Form',
	'form-paragraph' => 'If you are interested on hiring me, have any doubts, require consulting or are interested in creating something together, please fill this form to get in touch',

	'form-paragraph2' => '<p>Fields marked with asterisk(<span style="color: red">*</span>)
						   	are required</p>',

	'form-field-name' => 'Name',
	'form-field-phone' => 'Phone',
	'form-field-email' => '@Email',
	'form-field-emailPlaceholder'=>'user@domain.ex',
	'form-field-message' => 'Message',
	'form-field-messagePlaceholder' => 'Please type your message',

	'form-sendButton' => 'Send',

	'storedModal-Title' => 'Stored Request',
	'storedModal-Paragraph' => '<p>
									request has been succesully stored.
									<br>
									i\'ll receive a notification
									and get in touch with you as soon as possible through means provided (or via provided mail by default)
								</p>',

	'storedModal-closeButton' => 'Close',
];