<?php

return [

	/*
	|--------------------------------------------------------------------------
	| App Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used within header (navigation)
	|
	*/

	/*Header/Navbar Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'nav-home' => 'Home',
	'nav-blog' => 'Blog',
	'nav-contact' => 'Contact',

	/*Footer Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'footer-paragraph1' => 'Follow me on social networks and platforms',
	'footer-paragraph2' => 'Designed and developed with',

];