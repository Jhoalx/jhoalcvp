<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Home Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used within home page (index)
	|
	*/

	/*Welcome Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'welcome-profession'      => 'Software Developer',
	'welcome-contact_me'      => 'Contact me',

	/*SideNav Links*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'sideNav-about_me'        => 'About me',
	'sideNav-skills'          => 'Skills',
	'sideNav-resume'          => 'Resume / Curriculum Vitae',
	'sideNav-education'       => 'Education',
	'sideNav-work_experience' => 'Work Experience',
	'sideNav-services'        => 'Services',
	'sideNav-hobbies'         => 'Hobbies',


	/*About Me Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'about_me-sectionTitle'   => '¡Hi!',
	'about_me-paragraph1'     => 'My name is John Alejandro Zapata, colombian, resident in Medellín (Antioquia),
								i\'m a software developer by profession and have more than 14 years of work experience								
								in technology and administration areas (that\'s it, i work officially since i have 16)
								and i\'m here to help you with your computing world requirements.',

	'about_me-paragraph2'      => '<b>Let\'s build something together!</b><br>
									If you\'re interested in working as a team, come on!<br>',
	'about_me-paragraph2-link' => 'Get in touch!',

	'about_me-paragraph3'         => 'You\'re invited to keep browsing this site so you get to know a little more about me',

	/*Accordion - Headers*/
	'about_me-aptitudes-header'   => 'Aptitudes',
	'about_me-attitudes-header'   => 'Attitudes',
	'about_me-objectives-header'  => 'Objectives',
	'about_me-competences-header' => 'Competences',


	/*Accordion - content*/
	'about_me-aptitudes-content'  => 'High analytic capacity <br>
									Risk assumption <br>
									Iniciative <br>
									Automotivation <br>
									Critical capacity <br>
									Creativity <br>
									Integrity <br>
									Persuasive communication <br>
									Commitment <br>
									Decision <br>
									Flexibility <br>
									Adaptability <br>
									Independence <br>
									Leadership <br>',

	'about_me-attitudes-content' => 'Kindness <br>
									Teamwork <br>
									Cordiality <br>
									Respect <br>
									Proactivity <br>
									Tolerance <br>
									Optimism <br>
									Open Mind <br>
									Responsibility <br>
									Entrepreneurial attitude <br>',

	'about_me-objectives-content' => 'Learn new thigs every day <br>
									Make more friends<br>
									Start own company <br>
									Do what i love <br>
									Success <br>
									Studying career abroad <br>',

	'about_me-competences-content' => 'High English level <br>
									Autonomous Learning <br>
									Excelent work experience <br>',

	/*Languages*/
	'about_me-languages'           => 'Languages',
	'about_me-languages-es'        => '<p>Spanish <br> (Native)</p>',
	'about_me-languages-en'        => '<p>English <br> (90%)</p>',



	/*Skills Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'skills-sectionTitle'          => 'skills',

	'skills-softwareDevelopmet'    => 'Software Development',
	'skills-operatingSystems'      => 'Operating systems',
	'skills-graphicDesign/Edition' => 'Graphic Design/Edition',
	'skills-administration'        => 'Administration',

	'skills-detailsButton'           => 'Details',
	'skills-modal-closeButton'       => 'Close',

	/*skills modals*/
	'skills-modal-obtained/Advanced' => 'Obtained / Advanced',
	'skills-modal-studying' => 'Currently Learning',
	'skills-modal-toImprove/Obtain'  => 'For Improvement / Obtaining',

	'skills-modal-responsiveWebDesign' => 'Responsive Web Design',
	'skills-modal-databases'           => 'Databases',


	/*Resume / Curriculum Vitae Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'resume-sectionTitle'              => 'Download<br>resume',
	'resume-paragraph'                 => 'Contains info about my education, knowledge, contact, among others',


	/*Education Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'education-sectionTitle'           => 'Education',
	'education-title'                  => 'Software Development Technician',
	'education-paragraph'              => 'I am able to develop computer programs, applied to activities and
											business strategies. I can perform analysis, design, implementation and maintenance 
											of applications in web environment, desktop and mobile devices.',

	/*Experience Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'experience-sectionTitle' => 'work experience',
	'experience-paragraph'    => 'Notice: it\'s not full history',

	/*Services Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'services-sectionTitle' => 'Services',

	'services-detailsButton'     => 'Details',
	'services-modal-closeButton' => 'Close',

	'services-softwareDevelopment'                => 'Software Development',
	'services-technicalService'                   => 'Tech Service',
	'services-importFromUSA'                      => 'Import from USA',
	'services-consulting'                         => 'Consulting',

	/*services modals*/
	'services-modal-sofwareDevelopment-paragraph' => '	<p>
															I design and implement customized solutions for people and SMEs using the
															traditional application development model: <br>
														</p>
														<ul>
															<li>System Analysis</li>
															<li>Requirement gathering</li>
															<li>Design</li>
															<li>Coding</li>
															<li>Testing</li>
															<li>Maintenance</li>
														</ul>',

	'services-modal-technicalService-paragraph' => 'I carry out physical, logical, corrective and preventive maintenance and repair for your computers (s)',
	'services-importFromUSA-paragraph'          => '<p>Need something and can\'t find it? <br>
 														I can help you make the purchase and import you want
														from any store within united states
													</p>',

	'services-consulting-paragraph' => 'I perceive your disadvantages and / or problems, analyze and organize your information and
										provide processed data in order to make innovative decisions,
										ensuring to generate a positive impact in your processes or implementations,
										the above completely under your terms of privacy ',


	/*Hobbies Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'hobbies-sectionTitle' => 'Hobbies',
	'hobbies-sport' => 'Sport',
	'hobbies-reading' => 'Reading',
	'hobbies-gamingPCs' => 'Gaming PC\'s',
	'hobbies-programming' => 'Programming',
	'hobbies-videogames' => 'Videogames',
	'hobbies-movies' => 'Movies',
	'hobbies-music' => 'Music',
];
