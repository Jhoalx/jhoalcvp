<?php

return [

	/*
	|--------------------------------------------------------------------------
	| App Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used within header (navigation)
	|
	*/

	/*Header/Navbar Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'nav-home' => 'Inicio',
	'nav-blog' => 'Blog',
	'nav-contact' => 'Contacto',

	/*Footer Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'footer-paragraph1' => 'Sígueme en redes sociales y plataformas',
	'footer-paragraph2' => 'Diseñado y Desarrollado con:',
];