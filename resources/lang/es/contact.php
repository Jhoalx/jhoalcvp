<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Contact Page Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used within Contact Form Viewq
	|
	*/

	'form-sectionTitle' => 'Formulario de Contacto',
	'form-paragraph' => 'Si estas interesado en contratar mis servicios, tines dudas, requieres asesoría o estas
							interesdo en crear algo juntos, porfavor utiliza el siguiente formulario para establecer contacto:',

	'form-paragraph2' => '<p>Los campos marcados con asterisco (<span style="color: red">*</span>)
						   	son requeridos</p>',

	'form-field-name' => 'Nombre',
	'form-field-phone' => 'Teléfono',
	'form-field-email' => '@Email',
	'form-field-emailPlaceholder'=>'usuario@dominio.ej',
	'form-field-message' => 'Mensaje',
	'form-field-messagePlaceholder' => 'Porfavor escriba su mensaje',

	'form-sendButton' => 'Enviar',

	'storedModal-Title' => 'Solicitud Almacenada',
	'storedModal-Paragraph' => '<p>
									Se almacenó la solicitud de contacto satisfactoriamente.
									<br>
									pronto recibiré una notificación.
									en cuanto pueda me comunicaré con usted mediante los medios proporcionados (o via este correo electronico en su defecto)
								</p>',

	'storedModal-closeButton' => 'Cerrar',
];