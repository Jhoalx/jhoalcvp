<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Home Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used within home page (index)
	|
	*/

	/*Welcome Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'welcome-profession'      => 'Desarrollador de Software',
	'welcome-contact_me'      => 'Contactame',

	/*SideNav Links*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'sideNav-about_me'        => 'Sobre Mi',
	'sideNav-skills'          => 'Habilidades',
	'sideNav-resume'          => 'Hoja de Vida',
	'sideNav-education'       => 'Educación',
	'sideNav-work_experience' => 'Experiencia Laboral',
	'sideNav-services'        => 'Servicios',
	'sideNav-hobbies'         => 'Pasatiempos',


	/*About Me Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'about_me-sectionTitle'   => '¡Hola!',
	'about_me-paragraph1'     => 'Mi nombre es John Alejandro Zapata, colombiano, residente en Medellín (Antioquia),
								soy desarrollador de software de profesión y
								tengo más de 14 años de experiencia en áreas de tecnología y administración (así es, laboro
								oficialmente desde que tengo 16)
								y estoy aquí para ayudarte con tus requerimientos en el mundo de la Informática.',

	'about_me-paragraph2'      => '<b>¡Creemos Algo Juntos!</b><br>
									¡Si te interesa que trabajemos en equipo, vamos!, <br>',
	'about_me-paragraph2-link' => 'Ponte en Contacto',

	'about_me-paragraph3'         => 'Te invito a seguir leyendo este sitio para que conozcas un poco más sobre mí.',

	/*Accordion - Headers*/
	'about_me-aptitudes-header'   => 'Aptitudes',
	'about_me-attitudes-header'   => 'Actitudes',
	'about_me-objectives-header'  => 'Objetivos',
	'about_me-competences-header' => 'Competencias',

	/*Accordion - content*/
	'about_me-aptitudes-content'  => 'Alta capacidad analítica <br>Asunción de Riesgos <br>
									Iniciativa <br>
									Automotivación <br>
									Capacidad Crítica <br>
									Creatividad <br>
									Integridad <br>
									Comunicación Persuasiva <br>
									Compromiso <br>
									Decisión <br>
									Flexibilidad <br>
									Adaptabilildad <br>
									Independencia <br>
									Liderazgo <br>
									<br>',

	'about_me-attitudes-content' => 'Amabilidad <br>
									Trabajo en equipo <br>
									Cordialidad <br>
									Respeto <br>
									Proactividad <br>
									Tolerancia <br>
									Optimismo <br>
									Mente Abierta <br>
									Responsabilidad <br>
									Actitud emprendedora <br>',

	'about_me-objectives-content' => 'Aprender nuevas cosas cada día <br>
									Hacer más amistades <br>
									Fundar empresa propia o en compañia <br>
									Hacer lo que me apasiona <br>
									Tener Éxito <br>
									Estudiar carrera profesional en el exterior <br>',

	'about_me-competences-content' => 'Idioma Inglés Alto <br>
									Aprendizaje Autónomo <br>
									Excelente experiencia laboral <br>',

	/*Languages*/
	'about_me-languages'           => 'Idiomas',
	'about_me-languages-es'        => '<p>Español <br> (Nativo)</p>',
	'about_me-languages-en'        => '<p>Inglés <br> (90%)</p>',


	/*Skills Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'skills-sectionTitle'          => 'habilidades',

	'skills-softwareDevelopmet'    => 'Desarrollo de Software',
	'skills-operatingSystems'      => 'Sistemas Operativos',
	'skills-graphicDesign/Edition' => 'Diseño/Edición Gráfica',
	'skills-administration'        => 'Administración',

	'skills-detailsButton'           => 'Detalles',
	'skills-modal-closeButton'       => 'Cerrar',

	/*skills modals*/
	'skills-modal-obtained/Advanced' => 'Obtenidas / Avanzadas',
	'skills-modal-studying' => 'Aprendiendo Actualmente',
	'skills-modal-toImprove/Obtain'  => 'Por Mejorar / Obtener',

	'skills-modal-responsiveWebDesign' => 'Diseño Web Adaptable / Responsive',
	'skills-modal-databases'           => 'Bases de Datos',


	/*Resume / Curriculum Vitae Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'resume-sectionTitle'              => 'DESCARGAR<br>HOJA DE VIDA',
	'resume-paragraph'                 => 'Contiene información sobre mi educación, conocimientos, contacto, entre otros',


	/*Education Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	'education-sectionTitle'           => 'Educación',
	'education-title'                  => 'Técnico en Desarrollo de Software',
	'education-paragraph'              => 'Estoy en capacidad de desarrollar programas para computador, aplicados a las actividades y
										   estrategias empresariales. puedo realizar el análisis, diseño e implementación de aplicaciones y
										   el mantenimiento de programas en ambiente web, escritorio y para dispositivos móviles.',

	/*Experience Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'experience-sectionTitle' => 'EXPERIENCIA LABORAL',
	'experience-paragraph'    => 'Nota: no es historial completo',

	/*Services Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'services-sectionTitle' => 'Servicios',
	'services-paragraph'    => 'Nota: no es historial completo',

	'services-detailsButton'     => 'Detalles',
	'services-modal-closeButton' => 'Cerrar',

	'services-softwareDevelopment'                => 'Desarrollo de Software',
	'services-technicalService'                   => 'Servicio Técnico',
	'services-importFromUSA'                      => 'Importación de U.S.A',
	'services-consulting'                         => 'Asesoría / Consultoría',

	/*services modals*/
	'services-modal-sofwareDevelopment-paragraph' => '	<p>
															Diseño e implemento soluciones a la medida para personas y pymes mediante el modelo
															tradicional de desarrollo de aplicaciones: 
															<br>
														</p>
														<ul>
															<li>Analisis de sistema</li>
															<li>Levantamiento de requisitos</li>
															<li>Diseño</li>
															<li>Codificación</li>
															<li>Pruebas</li>
															<li>Mantenimiento</li>
														</ul>',

	'services-modal-technicalService-paragraph' => 'Llevo a cabo reparación y mantenimiento Físico, Lógico, correctivo, preventivo para tu(s) equipo(s)',
	'services-importFromUSA-paragraph'          => '<p>Necesitas un producto y no puedes encontrarlo? <br>
 														Puedo ayudarte a realizar la compra e importacion deseada
														desde cualquier tienda en estados unidos
													</p>',

	'services-consulting-paragraph' => 'Percibo sus inconvenientes y/o problemas, Analizo y Organizo su información y
										proporciono datos procesados para la toma de desiciones innovativas,
										asegurandome de generar un impacto positivo ya sea en tus procesos o implementaciones,
										lo anterior completamente bajo clausulas de privacidad de la información y sus productos',


	/*Hobbies Section*/
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	'hobbies-sectionTitle' => 'Pasatiempos',
	'hobbies-sport' => 'Deporte',
	'hobbies-reading' => 'Lectura',
	'hobbies-gamingPCs' => 'PCs Gamer',
	'hobbies-programming' => 'Programar',
	'hobbies-videogames' => 'Videojuegos',
	'hobbies-movies' => 'Cine',
	'hobbies-music' => 'Música',
];
