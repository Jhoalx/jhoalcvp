@extends('layouts.app')


{{--  Defino el id del body hacia el layout master  --}}
@section('body-id', 'contacto')

{{--  El contenido  --}}
@section('content')
	<div class="container py-5">
		<div class="card">
			<div class="card-header">
				<h3>{{ trans('contact.form-sectionTitle') }}</h3>
			</div>
			<div class="card-body">
				<form method="POST" action="{{ url('contacto') }}">
					{{ csrf_field() }}

					<div class="py-2">
						<p class="card-text">{{ trans('contact.form-paragraph') }}</p>

						{!! trans('contact.form-paragraph2') !!}
					</div>
					<div class="form-group row">
						<label for="name" class="col-sm-2 col-form-label">{{ trans('contact.form-field-name') }}<span style="color: red">*</span></label>

						<div class="col-md-10">
							<input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
								   name="name" value="{{ old('name') }}" required>

							@if ($errors->has('name'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="phone" class="col-sm-2 col-form-label">{{ trans('contact.form-field-phone') }}</label>

						<div class="col-md-10">
							<input id="phone" type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}"
								   name="phone" value="{{ old('phone') }}" placeholder="+57">

							@if ($errors->has('phone'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('phone') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="email" class="col-sm-2 col-form-label">{{ trans('contact.form-field-email') }}<span style="color: red">*</span></label>

						<div class="col-md-10">
							<input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
								   name="email" value="{{ old('email') }}"
								   required placeholder="{{ trans('contact.form-field-emailPlaceholder') }}">

							@if ($errors->has('email'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="message" class="col-sm-2 col-form-label">{{ trans('contact.form-field-message') }}<span style="color: red">*</span></label>

						<div class="col-md-10">
							<textarea id="message" name="message" rows="5" required
									  class="form-control {{ $errors->has('message') ? ' is-invalid' : '' }}"
									  placeholder="{{ trans('contact.form-field-messagePlaceholder') }}">{{ old('message') }}</textarea>

							@if ($errors->has('message'))
								<span class="invalid-feedback">
								<strong>{{ $errors->first('message') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<div class="mx-auto">
							<button type="submit" class="btn btn-primary">{{ trans('contact.form-sendButton') }}</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	@if(session('status') == 'ContactStored')
		<div class="modal fade" id="modalContactStored" tabindex="-1" role="dialog"
			 aria-labelledby="ModalContactStored" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">{{ trans('contact.storedModal-Titlee') }}</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
									aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
							<div class="row">
								{!! trans('contact.storedModal-Paragraph') !!}
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('contact.storedModal-closeButton') }}</button>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$('#modalContactStored').modal('show');
		</script>
	@endif

@stop