<!DOCTYPE html>
<html lang="es">
	@include('layouts.partials.head')

<body id="@yield('body-id')">
	@include('layouts.partials.header')


	@yield('content')


	@include('layouts.partials.footer')
	<script src="{{ asset('/js/main.js') }}" type="text/javascript"></script>
</body>
</html>