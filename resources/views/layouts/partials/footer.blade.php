<footer class="pt-3 text-center" id="pie-de-pagina">
	<div class="container-fluid" id="JhoalInfo">
		<div class="container">
			<p>
				© John Alejandro Zapata - 2017 <br>Medellín - Colombia <br>
				+57 312 781 4422
			</p>
			<a href="mailto:jhoalx@gmail.com" title="Jhoalx@gmail.com &lt;br&gt;">Jhoalx@gmail.com</a>
			<p>{{ trans('app.footer-paragraph1') }}</p>
			<ul class="col-lg-6 mx-auto" id="redes-sociales">
				<li>
					<a href="https://www.facebook.com/jhoalx">
						<i class="fa fa-facebook-square"></i>
					</a>
				</li>
				<li>
					<a href="https://twitter.com/jhoalz">
						<i class="fa fa-twitter"></i>
					</a>
				</li>
				<li>
					<a href="https://www.twitch.tv/jhoalx">
						<i class="fa fa-twitch"></i>
					</a>
				</li>
				<li>
					<a href="https://www.youtube.com/channel/UCoNGsQ55COEPA5aJzXZR0wA">
						<i class="fa fa-youtube"></i>
					</a>
				</li>
				<li>
					<a href="https://www.instagram.com/jhoalx/">
						<i class="fa fa-instagram"></i>
					</a>
				</li>
				<li>
					<a href="https://github.com/jhoalx">
						<i class="fa fa-github"></i>
					</a>
				</li>
				<li>
					<a href="https://bitbucket.org/Jhoalx">
						<i class="fa fa-bitbucket"></i>
					</a>
				</li>
				<li>
					<a href="https://www.goodreads.com/user/show/48332177-jhon-zapata" title="GoodReads">
						<i class="fa fa-book"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="container-fluid" id="SiteInfo">
		<div class="container">
			<div class="row">
				<div class="col-5 d-flex align-content-center">
					<img src="{{ asset('/images/AvatarJhoal.jpg') }}" alt="Avatar Jhoal">
				</div>
				<div class="col-7 d-flex flex-column justify-content-end align-items-end p-3">
					<p>{{ trans('app.footer-paragraph2') }}</p>
					<ul id="devtools">
						<li>
							<a href="https://laravel.com/">
								<img src="{{ asset('/images/laravel.svg') }}" alt="Laravel">
							</a>
						</li>
						<li>
							<a href="https://www.w3.org/TR/html5/">
								<img src="{{ asset('/images/HTML5.svg') }}" alt="HTML5">
							</a>
						</li>
						<li>
							<a href="https://www.w3.org/Style/CSS/">
								<img src="{{ asset('/images/CSS3.svg') }}" alt="CSS3">
							</a>
						</li>
						<li>
							<a href="http://jadelang.net/">
								<img src="{{ asset('/images/pugjs.svg') }}" alt="Pug (ex Jade)">
							</a>
						</li>
						<li>
							<a href="http://getbootstrap.com/">
								<img src="{{ asset('/images/Bootstrap4.svg') }}" alt="Bootstrap 4">
							</a>
						</li>
						<li>
							<a href="http://sass-lang.com/">
								<img src="{{ asset('/images/Sass.svg') }}" alt="Sass">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>