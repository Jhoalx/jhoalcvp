<div class="header-wrapper {{ Request::is('/') ? 'd-none' : '' /*Se muestra por JS(main.js)*/ }}">
	<header>
		<nav class="navbar navbar-expand-md">
			<div class="container">
				<a class="navbar-brand" href="./">
					<img class="logo" src="{{ asset('images/logo.svg') }}" alt="Logo Sitio">
				</a>
				@foreach ( array_keys( config('app.locales')) as $lang)
					<a class="nav-item nav-link" href="{{ url('lang', $lang) }}">
						<img src="{{ asset('/images/flag-') . $lang . '.svg' }}">
					</a>
				@endforeach
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
						data-target="#menu-navegacion" aria-expanded="false" aria-label="Toggle navigation">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button>
				<div class="collapse navbar-collapse" id="menu-navegacion">
					<div class="container text-right" id="contenedor-menu-navegacion">
						<a class="nav-item nav-link {{{ (Request::is('/') ? 'active' : '') }}}"
						   href="{{ route('home') }}">{{ trans('app.nav-home') }}
							<i class="fa fa-home"></i>
						</a>
						<a class="nav-item nav-link disabled {{{ (Request::is('blog') ? 'active' : '') }}}"
						   href="{{route('blog')}}">{{trans('app.nav-blog')}}
							<i class="fa fa-pencil-square-o"></i>
						</a>
						<a class="nav-item nav-link {{{ (Request::is('contacto') ? 'active' : '') }}}"
						   href="{{route('contact')}}">{{ trans('app.nav-contact') }}
							<i class="fa fa-address-book-o"></i>
						</a>
					</div>
				</div>
			</div>
		</nav>
	</header>
</div>