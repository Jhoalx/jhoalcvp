@extends('layouts.app')


{{--  Defino el id del body hacia el layout master  --}}
@section('body-id', 'index')

{{--  El contenido  --}}
@section('content')


	<section class="d-flex flex-column align-items-center justify-content-around" id="bienvenida">
		<div class="container-fluid d-flex flex-column text-center" id="texto-encabezado">
			<!--i.fa.fa-hourglass-2(style="font-size: 75px; text-shadow: 2px 2px 5px red")-->
			<!--h3(style="text-shadow: 2px 2px 5px red") Sitio en Construcción-->
			<h1>John Alejandro Zapata.</h1>
			<p>{{ trans('home.welcome-profession') }}</p>
		</div>
		<a class="btn btn-primary btn-lg" href="{{ route('contact') }}">{{trans('home.welcome-contact_me')}}</a>
		<div class="flecha-bajar">
			<a href="#sobre-mi">
				<i class="fa fa-chevron-down" aria-hidden="true"></i>
			</a>
		</div>
	</section>
	<div id="navegacion-lateral">
		<button type="button" data-toggle="collapse" data-target="#menu-navegacion-lateral" aria-expanded="false"
				aria-label="Toggle navigation">
			<i class="fa fa-bars" aria-hidden="true"></i>
		</button>
		<div class="collapse" id="menu-navegacion-lateral">
			<a class="nav-item nav-link" href="#sobre-mi" data-toggle="tooltip" data-placement="right"
			   title="{{ trans('home.sideNav-about_me') }}">
				{{ trans('home.sideNav-about_me') }}
				<i class="fa fa fa-user-circle-o"></i>
			</a>
			<a class="nav-item nav-link" href="#habilidades" data-toggle="tooltip" data-placement="right"
			   title="{{trans('home.sideNav-skills')}}">{{trans('home.sideNav-skills')}}
				<i class="fa fa-gear"></i>
			</a>
			<a class="nav-item nav-link" href="#hoja-de-vida" data-toggle="tooltip" data-placement="right"
			   title="{{trans('home.sideNav-resume')}}">{{trans('home.sideNav-resume')}}
				<i class="fa fa-download"></i>
			</a>
			<a class="nav-item nav-link" href="#educacion" data-toggle="tooltip" data-placement="right"
			   title="{{trans('home.sideNav-education')}}">{{trans('home.sideNav-education')}}
				<i class="fa fa-university"></i>
			</a>
			<a class="nav-item nav-link" href="#experiencia-laboral" data-toggle="tooltip" data-placement="right"
			   title="{{trans('home.sideNav-work_experience')}}">{{trans('home.sideNav-work_experience')}}
				<i class="fa fa-black-tie"></i>
			</a>
			<a class="nav-item nav-link" href="#servicios" data-toggle="tooltip" data-placement="right"
			   title="{{trans('home.sideNav-services')}}">{{trans('home.sideNav-services')}}
				<i class="fa fa-handshake-o"></i>
			</a>
			<a class="nav-item nav-link" href="#hobbies" data-toggle="tooltip" data-placement="right"
			   title="{{trans('home.sideNav-hobbies')}}">
				{{trans('home.sideNav-hobbies')}}
				<i class="fa fa-heartbeat"></i>
			</a>
		</div>
	</div>
	<section class="py-5" id="sobre-mi">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 d-flex justify-content-center align-items-center">
					<img class="img-thumbnail" src="{{ asset('/images/foto.jpg') }}"
						 alt="Foto John Alejandro Zapata Roldan">
				</div>
				<div class="col-12 col-md-6">
					<h2 class="text-center text-md-left pb-3">{{ trans('home.about_me-sectionTitle') }}</h2>
					<p class="text-justify">
						{{trans('home.about_me-paragraph1')}}
					</p>
					<p>
						{!! trans('home.about_me-paragraph2')!!}
						<a href="{{ route('contact') }}">{{ trans('home.about_me-paragraph2-link') }}</a>
					</p>
					<p>{{ trans('home.about_me-paragraph3') }}</p>
					<div class="col-12 col-md-10 mx-auto" id="accordion-AAOC" role="tablist"
						 aria-multiselectable="true">
						<div class="card">
							<div class="card-header" id="headingAptitudes" role="tab" data-toggle="collapse"
								 data-parent="#accordion-AAOC" href="#collapseAptitudes">
								<h5 class="mb-0">
									<a data-toggle="collapse" data-parent="#accordion-AAOC" href="#collapseAptitudes"
									   aria-expanded="true"
									   aria-controls="collapseAptitudes">{{trans('home.about_me-aptitudes-header')}}
									</a>
								</h5>
							</div>
							<div class="collapse" id="collapseAptitudes" role="tabpanel" aria-labelledby="headingOne">
								<div class="card-block">
									{!! trans('home.about_me-aptitudes-content') !!}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingActitudes" role="tab" data-toggle="collapse"
								 data-parent="#accordion-AAOC" href="#collapseActitudes">
								<h5 class="mb-0">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-AAOC"
									   href="#collapseActitudes" aria-expanded="false" aria-controls="collapseTwo">
										{{trans('home.about_me-attitudes-header')}}
									</a>
								</h5>
							</div>
							<div class="collapse" id="collapseActitudes" role="tabpanel" aria-labelledby="headingTwo">
								<div class="card-block">
									{!! trans('home.about_me-attitudes-content') !!}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingObjetivos" role="tab" data-toggle="collapse"
								 data-parent="#accordion-AAOC" href="#collapseLogros">
								<h5 class="mb-0">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-AAOC"
									   href="#collapseLogros" aria-expanded="false" aria-controls="collapseThree">
										{{ trans('home.about_me-objectives-header') }}
									</a>
								</h5>
							</div>
							<div class="collapse" id="collapseLogros" role="tabpanel" aria-labelledby="headingThree">
								<div class="card-block">
									{!! trans('home.about_me-objectives-content') !!}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingAmbiciones" role="tab" data-toggle="collapse"
								 data-parent="#accordion-AAOC" href="#collapseAbiciones">
								<h5 class="mb-0">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-AAOC"
									   href="#collapseAbiciones" aria-expanded="false" aria-controls="collapseThree">
										{{ trans('home.about_me-competences-header') }}
									</a>
								</h5>
							</div>
							<div class="collapse" id="collapseAbiciones" role="tabpanel" aria-labelledby="headingThree">
								<div class="card-block">
									{!! trans('home.about_me-competences-content') !!}
								</div>
							</div>
						</div>
					</div>
					<h3 class="text-center mt-4">{{ trans('home.about_me-languages') }}</h3>
					<div class="container d-flex text-center" id="idiomas">
						<div class="col-6">
							{!! trans('home.about_me-languages-es') !!}
							<img src="{{ asset('images/flag-es.svg') }}" alt="Español">
						</div>
						<div class="col-6">
							{!! trans('home.about_me-languages-en') !!}
							<img src="{{ asset('images/flag-en.svg') }}" alt="Inglés">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="py-5" id="habilidades">
		<div class="container-fluid py-md-5">
			<div class="row">
				<h2 class="col-md-12 p-md-0 text-center">{{ trans('home.skills-sectionTitle') }}</h2>
			</div>
			<div class="row">
				<article class="col-12 col-md-6">
					<div class="hexagonoblanco">
						<i class="fa fa-file-code-o" aria-hidden="true"></i>
					</div>
					<h3>{{ trans('home.skills-softwareDevelopmet') }}</h3>
					<button class="btn btn-primary" type="button" data-toggle="modal"
							data-target="#modalHabilidadesDev">{{ trans('home.skills-detailsButton') }}
					</button>
				</article>
				<article class="col-12 col-md-6">
					<div class="hexagonoblanco">
						<i class="fa fa-windows" aria-hidden="true"></i>
					</div>
					<h3>{{ trans('home.skills-operatingSystems') }}</h3>
					<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalHabilidadesOS">
						{{ trans('home.skills-detailsButton') }}
					</button>
				</article>
			</div>
			<div class="row">
				<article class="col-12 col-md-6">
					<div class="hexagonoblanco">
						<i class="fa fa-image" aria-hidden="true"></i>
					</div>
					<h3>{{ trans('home.skills-graphicDesign/Edition') }}</h3>
					<button class="btn btn-primary" type="button" data-toggle="modal"
							data-target="#modalHabilidadesDesign">{{ trans('home.skills-detailsButton') }}
					</button>
				</article>
				<article class="col-12 col-md-6">
					<div class="hexagonoblanco">
						<i class="fa fa-black-tie" aria-hidden="true"></i>
					</div>
					<h3>{{ trans('home.skills-administration') }}</h3>
					<button class="btn btn-primary" type="button" data-toggle="modal"
							data-target="#modalHabilidadesAdmin">{{ trans('home.skills-detailsButton') }}
					</button>
				</article>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<section class="col-md-4 ml-auto my-5" id="hoja-de-vida">
				<div class="container">
					<div class="card">
						<img class="card-img-top" src="{{ asset('/images/MiniaturaCV.jpg') }}"
							 alt="Miniatura Hoja de Vida">
						<div class="card-block p-3 text-center">
							<h2 class="card-title">{!!  trans('home.resume-sectionTitle') !!}</h2>
							<p class="card-text">{{ trans('home.resume-paragraph') }}</p>
							<a href="{{ asset('/files/CV_JohnAlejandroZapata.pdf') }}">
								<i class="fa fa-download"></i>
							</a>
						</div>
					</div>
				</div>
			</section>
			<section class="col-md-6 mr-auto text-center py-5 my-md-5" id="educacion">
				<div class="container">
					<h2>{{ trans('home.education-sectionTitle') }}</h2>
					<i class="fa fa-university"></i>
					<h3>{{ trans('home.education-title') }}</h3>
					<p>{{ trans('home.education-paragraph') }}</p>
				</div>
			</section>
		</div>
	</div>
	<section class="py-5" id="experiencia-laboral">
		<div class="container text-center">
			<h2>{{ trans('home.experience-sectionTitle') }}</h2>
			<p>{{ trans('home.experience-paragraph') }}</p>
			<div class="row col-12 col-sm-8 col-md-12 mx-auto">
				{{--TODO: English Images--}}
				<img class="d-block d-md-none" src="{{ asset('/images/ExperienciaLaboralVertical.svg') }}"
					 alt="Imagen Experiencia Laboral Vertical">
				<img class="d-none d-md-block" src="{{ asset('/images/ExperienciaLaboral.svg') }}"
					 alt="Imagen Experiencia Laboral">
			</div>
		</div>
	</section>
	<main class="py-5" id="servicios">
		<div class="container d-flex flex-column col-md-9 col-lg-8">
			<h2 class="text-center pb-2">{{ trans('home.services-sectionTitle') }}</h2>
			<article>
				<div class="row mr-auto">
					<div class="hexagonoblanco col-3">
						<i class="fa fa-file-code-o" aria-hidden="true"></i>
					</div>
					<div class="container col-9">
						<h3>{{ trans('home.services-softwareDevelopment') }}</h3>
						<button class="btn btn-primary" type="button" data-toggle="modal"
								data-target="#modalServiciosDev">{{ trans('home.services-detailsButton') }}
						</button>
					</div>
				</div>
			</article>
			<article>
				<div class="row ml-auto">
					<div class="hexagonoblanco col-3">
						<i class="fa fa-wrench" aria-hidden="true"></i>
					</div>
					<div class="container col-9">
						<h3>{{ trans('home.services-technicalService') }}</h3>
						<button class="btn btn-primary" type="button" data-toggle="modal"
								data-target="#modalServiciosTecnicos">{{ trans('home.services-detailsButton') }}
						</button>
					</div>
				</div>
			</article>
			<article>
				<div class="row mr-auto">
					<div class="hexagonoblanco col-3">
						<i class="fa fa-gift" aria-hidden="true"></i>
					</div>
					<div class="container col-9">
						<h3>{{ trans('home.services-importFromUSA') }}</h3>
						<button class="btn btn-primary" type="button" data-toggle="modal"
								data-target="#modalServiciosImportacion">{{ trans('home.services-detailsButton') }}
						</button>
					</div>
				</div>
			</article>
			<article>
				<div class="row ml-auto">
					<div class="hexagonoblanco col-3">
						<i class="fa fa-graduation-cap" aria-hidden="true"></i>
					</div>
					<div class="container col-9">
						<h3>{{ trans('home.services-consulting') }}</h3>
						<button class="btn btn-primary" type="button" data-toggle="modal"
								data-target="#modalServiciosAsesoriaEnsenanza">{{ trans('home.services-detailsButton') }}
						</button>
					</div>
				</div>
			</article>
		</div>
	</main>
	<section class="py-5 text-center" id="hobbies">
		<div class="container">
			<h2 class="pb-3">{{ trans('home.hobbies-sectionTitle') }}</h2>
			<div class="owl-carousel">
				<div class="container">
					<h3>{{ trans('home.hobbies-sport') }}</h3><span><i class="fa fa-bicycle"></i></span>
				</div>
				<div class="container">
					<h3>{{ trans('home.hobbies-reading') }}</h3><span><i class="fa fa-book"></i></span>
				</div>
				<div class="container">
					<h3>{{ trans('home.hobbies-gamingPCs') }}</h3><span><i class="fa fa-desktop"></i></span>
				</div>
				<div class="container">
					<h3>{{ trans('home.hobbies-programming') }}</h3><span><i class="fa fa-slack"></i></span>
				</div>
				<div class="container">
					<h3>{{ trans('home.hobbies-videogames') }}</h3><span><i class="fa fa-gamepad"></i></span>
				</div>
				<div class="container">
					<h3>{{ trans('home.hobbies-movies') }}</h3><span><i class="fa fa-film"></i></span>
				</div>
				<div class="container">
					<h3>{{ trans('home.hobbies-music') }}</h3><span><i class="fa fa-headphones"></i></span>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="modalHabilidadesDev" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.skills-softwareDevelopmet') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<h4>{{ trans('home.skills-modal-obtained/Advanced') }}</h4>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/php.svg') }}" alt="Imágen PHP">
								<img src="{{ asset('/images/laravel.svg') }}" alt="Imágen Laravel">
							</div>
							<div class="col-4">
								<p>PHP 7 / Laravel</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="65"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/csharp.svg') }}" alt="Imágen C#">
							</div>
							<div class="col-4">
								<p>C# (MVC - Razor, WebForms)</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="65"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/CSS3.svg') }}" alt="Imágen Css3">
								<img src="{{ asset('/images/Sass.svg') }}" alt="Imágen SASS">
							</div>
							<div class="col-4">
								<p>CSS, SASS / SCSS</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="80"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/HTML5.svg') }}" alt="Imágen HTML5">
								<img src="{{ asset('/images/pugjs.svg') }}" alt="Imágen PUG / JADE">
							</div>
							<div class="col-4">
								<p>HTML5, Pug (ex Jade)</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="95"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/responsive-design.svg') }}" alt="imagen diseño responsive">
								<img src="{{ asset('/images/Bootstrap4.svg') }}" alt="Imagen Bootstrap">
							</div>
							<div class="col-4">
								<p>{{ trans('home.skills-modal-responsiveWebDesign') }} <br> (Twitter Bootstrap 4)</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="99"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/javascript.svg') }}" alt="imagen Javascript">
								<img src="{{ asset('/images/jquery.svg') }}" alt="imagen jQuery">
							</div>
							<div class="col-4">
								<p>Javascript, jQuery</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="75"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/microsoft-sql-server.svg') }}" alt="Imágen MS SQL Server">
								<img src="{{ asset('/images/MySQL.svg') }}" alt="Imágen MySQL Server">
							</div>
							<div class="col-4">
								<p>{{ trans('home.skills-modal-databases') }} <br> (Microsoft SQL Server, MySQL)</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="90"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/git.svg') }}" alt="Imágen Git">
							</div>
							<div class="col-4">
								<p>Git</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="80"></div>
							</div>
						</div>
						<h4 class="mt-5">{{ trans('home.skills-modal-studying') }}</h4>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/vue.svg') }}" alt="Imágen Vue JS">
							</div>
							<div class="col-4">
								<p>Vue.js 2</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="20"></div>
							</div>
						</div>
						<h4 class="mt-5">{{ trans('home.skills-modal-toImprove/Obtain') }}</h4>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/wordpress.svg') }}" alt="Imágen WordPress">
							</div>
							<div class="col-4">
								<p>WordPress</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="20"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/angular.svg') }}" alt="Imágen Angular">
							</div>
							<div class="col-4">
								<p>Angular 4</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="5"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.skills-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalHabilidadesOS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.skills-operatingSystems') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/windows.svg') }}" alt="Imágen Windows">
							</div>
							<div class="col-4">
								<p>Microsoft Windows</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="99"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/linux.svg') }}" alt="Imágen Linux">
							</div>
							<div class="col-4">
								<p>Linux</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="50"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/apple.svg') }}" alt="Imágen Apple">
							</div>
							<div class="col-4">
								<p>Apple Mac OS</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="50"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.skills-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalHabilidadesDesign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.skills-graphicDesign/Edition') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/photoshop.svg') }}" alt="Imágen Photoshop">
							</div>
							<div class="col-4">
								<p>Adobe Photoshop</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="65"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/illustrator.svg') }}" alt="Imágen Illustrator">
							</div>
							<div class="col-4">
								<p>Adobe Illustrator</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="55"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.skills-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalHabilidadesAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.skills-administration') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-4">
								<img src="{{ asset('/images/microsoftoffice.svg') }}" alt="Imágen office">
							</div>
							<div class="col-4">
								<p>Microsoft Office</p>
							</div>
							<div class="col-4">
								<div class="percircle small" data-percent="90"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.skills-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalServiciosDev" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.services-softwareDevelopment') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							{!! trans('home.services-modal-sofwareDevelopment-paragraph') !!}
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.services-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalServiciosTecnicos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.services-technicalService') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<p>{{ trans('home.services-modal-technicalService-paragraph') }}</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.services-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalServiciosImportacion" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.services-importFromUSA') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							{!! trans('home.services-importFromUSA-paragraph') !!}
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.services-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalServiciosAsesoriaEnsenanza" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">{{ trans('home.services-consulting') }}</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<p>
								{{ trans('home.services-consulting-paragraph') }}
							</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="button" data-dismiss="modal">{{ trans('home.services-modal-closeButton') }}</button>
				</div>
			</div>
		</div>
	</div>

	<a id="flecha-ir-arriba" href="#sobre-mi">
		<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
	</a>

@stop