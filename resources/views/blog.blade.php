@extends('layouts.app')


{{--  Defino el id del body hacia el layout master  --}}
@section('body-id', 'blog')

{{--  El contenido  --}}
@section('content')


	<div class="text-center" style="padding: 12rem">
		<br>
		<i class="fa fa-hourglass-2" style="font-size: 75px; text-shadow: 2px 2px 5px red;"></i>
		<h2 style="text-shadow: 2px 2px 5px red;">{{ trans('blog.blogSectionTitle') }}</h2>
	</div>

@stop