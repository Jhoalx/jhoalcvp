/*#################################################################
# Smooth Scrolling Inpage-Anchors
------------------------------------------------------------------*/
let scroll = new SmoothScroll('a[href*="#"]', {
	// Selectors
	ignore: '#accordion-AAOC a', // Selector for links to ignore (must be a valid CSS selector)
	header: null, // Selector for fixed headers (must be a valid CSS selector)

	// Speed & Easing
	speed       : 500, // Integer. How fast to complete the scroll in milliseconds
	offset      : 50, // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
	easing      : 'Ease-In-Out', // Easing pattern to use
	customEasing: function (time) {
	}, // Function. Custom easing pattern

	// Callback API
	before: function () {
	}, // Callback to run before scroll
	after : function () {
	} // Callback to run after scroll
});


/*#################################################################
   Scroll Functionality
 ------------------------------------------------------------------*/
$(window)
	.scroll(function () {

		//let height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		let scroll = $(window)
			.scrollTop();


		// SE DESHABILITA POR INCOMPATIBILIDAD CON CSS TRANSFORMS.
		// let navbarBrand = $(".navbar-brand");
		// let navbar = $(".navbar");
		//
		// //si estamos en el index
		// if (location.pathname === '/' || location.pathname === '/maqueta/' || location.pathname === '/jhoalcvp/') {
		//
		//
		// 	if (scroll >= height) {
		// 		$(navbar)
		// 			.addClass("fixed-top");
		//
		//
		// 		if (navbarBrand.css('opacity') === 0) {
		// 			$(navbarBrand)
		// 				.fadeTo(500, 1)
		// 				.clearQueue();
		// 		}
		// 	} else {
		// 		$(navbar)
		// 			.removeClass("fixed-top");
		//
		// 		if (navbarBrand.css('opacity') === 1) {
		// 			$(navbarBrand)
		// 				.fadeTo(500, 0)
		// 				.clearQueue();
		// 		}
		//
		// 		$("#sobre-mi")
		// 			.removeClass("mt-5");
		// 	}
		// } else {
		// 	$('.header-wrapper')
		// 		.removeClass('d-none');
		// 	$(navbar)
		// 		.addClass("fixed-top");
		// }


		if (scroll >= 75) {
			$("#navegacion-lateral")
				.addClass("fondo-menunavegacion-alterno");
		} else {
			$("#navegacion-lateral")
				.removeClass("fondo-menunavegacion-alterno");
		}


		if (scroll >= 50) {
			$("#flecha-ir-arriba")
				.fadeIn();
		} else {
			$("#flecha-ir-arriba")
				.fadeOut();
		}

	})
	.scroll();


/*#################################################################
# Owl Carousel
------------------------------------------------------------------*/
$('.owl-carousel')
	.owlCarousel({
		loop     : true,
		margin   : 0,
		autoWidth: false,
		autoplay : true,
		dotsEach : 1, // autoplayTimeout: 1500,

		navText: [
			'<i class="fa fa-arrow-circle-left" title="Anterior"></i>',
			'<i class="fa  fa-arrow-circle-right" title="Siguiente"></i>'
		],

		responsive: {
			0   : {
				items: 1,
				nav  : true,
				dots : false
			},
			500 : {
				items : 2,
				margin: 20,
				nav   : true,
				dots  : false
			},
			800 : {
				items : 3,
				margin: 20,
				dots  : true,
				nav   : false
			},
			1000: {
				items : 4,
				margin: 20,
				dots  : true,
				nav   : false
			}
		}
	});

/*#################################################################
# Popper.js (Tooltips)
------------------------------------------------------------------*/
$(function () {
	$('[data-toggle="tooltip"]')
		.tooltip();
});


/*#################################################################
# Waypoint Animations
------------------------------------------------------------------*/
$(function () {

	//swap bienvenida antes de header para el index
	$('#bienvenida')
		.insertBefore('.header-wrapper');
	$('.header-wrapper')
		.removeClass('d-none');

	let animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';


	$('h1')
		.waypoint(function () {
			$('h1')
				.addClass('animated flip')
				.one(animationEnd, function () {
					$('h1')
						.removeClass('animated flip');
				});
		}, {
			offset: '15%'
		});


	// Animacion Idioma Español
	let element1 = "#idiomas .col-6:first-child";
	let anim1 = "animated fadeInLeft";
	$(element1)
		.addClass('invisible');

	$(element1)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element1)
					.removeClass('invisible')
					.addClass(anim1)
					.one(animationEnd, function () {
						$(element1)
							.removeClass(anim1);
					});
			} else if (direction === 'up') {
				$(element1)
					.removeClass(anim1)
					.addClass('invisible');
			}
		}, {
			offset: 'bottom-in-view'
		});


	// Animacion Idioma Ingles
	let element2 = "#idiomas .col-6:nth-child(2)";
	let anim2 = "animated fadeInRight";
	$(element2)
		.addClass('invisible');

	$(element2)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element2)
					.removeClass('invisible')
					.addClass(anim2)
					.one(animationEnd, function () {
						$(element2)
							.removeClass(anim2);
					});
			} else if (direction === 'up') {
				$(element2)
					.removeClass(anim2)
					.addClass('invisible');
			}
		}, {
			offset: 'bottom-in-view'
		});


	// Animacion Habilidades h2
	let element3b = "#habilidades h2";
	let anim3b = "animated bounceIn";
	$(element3b)
		.addClass('invisible');

	$(element3b)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element3b)
					.removeClass('invisible')
					.addClass(anim3b)
					.one(animationEnd, function () {
						$(element3b)
							.removeClass(anim3b);
					});
			} else if (direction === 'up') {
				$(element3b)
					.removeClass(anim3b)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});


	// Animacion Habilidades 1
	let element3 = "#habilidades .row:nth-child(2) article:first-child";
	let anim3 = "animated rotateInDownLeft";
	$(element3)
		.addClass('invisible');

	$(element3)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element3)
					.removeClass('invisible')
					.addClass(anim3)
					.one(animationEnd, function () {
						$(element3)
							.removeClass(anim3);
					});
			} else if (direction === 'up') {
				$(element3)
					.removeClass(anim3)
					.addClass('invisible');
			}
		}, {
			offset: 'bottom-in-view'
		});


	// Animacion Habilidades 2
	let element4 = "#habilidades .row:nth-child(2) article:nth-child(2)";
	let anim4 = "animated rotateInDownRight";
	$(element4)
		.addClass('invisible');

	$(element4)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element4)
					.removeClass('invisible')
					.addClass(anim4)
					.one(animationEnd, function () {
						$(element4)
							.removeClass(anim4);
					});
			} else if (direction === 'up') {
				$(element4)
					.removeClass(anim4)
					.addClass('invisible');
			}
		}, {
			offset: 'bottom-in-view'
		});


	// Animacion Habilidades 3
	let element5 = "#habilidades .row:nth-child(3) article:first-child";
	let anim5 = "animated rotateInUpLeft";
	$(element5)
		.addClass('invisible');

	$(element5)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element5)
					.removeClass('invisible')
					.addClass(anim5)
					.one(animationEnd, function () {
						$(element5)
							.removeClass(anim5);
					});
			} else if (direction === 'up') {
				$(element5)
					.removeClass(anim5)
					.addClass('invisible');
			}
		}, {
			offset: '90%'
		});


	// Animacion Habilidades 4
	let element6 = "#habilidades .row:nth-child(3) article:nth-child(2)";
	let anim6 = "animated rotateInUpRight";
	$(element6)
		.addClass('invisible');

	$(element6)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element6)
					.removeClass('invisible')
					.addClass(anim6)
					.one(animationEnd, function () {
						$(element6)
							.removeClass(anim6);
					});
			} else if (direction === 'up') {
				$(element6)
					.removeClass(anim6)
					.addClass('invisible');
			}
		}, {
			offset: '90%'
		});


	// Animacion Hoja de Vida
	let element7 = "#hoja-de-vida";
	let anim7 = "animated zoomInRight";
	$(element7)
		.addClass('invisible');

	$(element7)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element7)
					.removeClass('invisible')
					.addClass(anim7)
					.one(animationEnd, function () {
						$(element7)
							.removeClass(anim7);
					});
			} else if (direction === 'up') {
				$(element7)
					.removeClass(anim7)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});


	// Animacion Educación
	let element8 = "#educacion";
	let anim8 = "animated zoomInLeft";
	$(element8)
		.addClass('invisible');

	$(element8)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element8)
					.removeClass('invisible')
					.addClass(anim8)
					.one(animationEnd, function () {
						$(element8)
							.removeClass(anim8);
					});
			} else if (direction === 'up') {
				$(element8)
					.removeClass(anim8)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});


	// Animacion Experiencia Laboral
	let element9 = "#experiencia-laboral";
	let anim9 = "animated slideInUp";
	$(element9)
		.addClass('invisible');

	$(element9)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element9)
					.removeClass('invisible')
					.addClass(anim9)
					.one(animationEnd, function () {
						$(element9)
							.removeClass(anim9);
					});
			} else if (direction === 'up') {
				$(element9)
					.removeClass(anim9)
					.addClass('invisible');
			}
		}, {
			offset: '75%'
		});


	// Animacion Servicios
	let element10 = "#servicios h2";
	let anim10 = "animated bounceIn";
	$(element10)
		.addClass('invisible');

	$(element10)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element10)
					.removeClass('invisible')
					.addClass(anim10)
					.one(animationEnd, function () {
						$(element10)
							.removeClass(anim10);
					});
			} else if (direction === 'up') {
				$(element10)
					.removeClass(anim10)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});


	// Animacion Servicios Dev
	let element11 = "#servicios article:nth-of-type(1)";
	let anim11 = "animated slideInRight";
	$(element11)
		.addClass('invisible');

	$(element11)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element11)
					.removeClass('invisible')
					.addClass(anim11)
					.one(animationEnd, function () {
						$(element11)
							.removeClass(anim11);
					});
			} else if (direction === 'up') {
				$(element11)
					.removeClass(anim11)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});


	// Animacion Servicios technician
	let element12 = "#servicios article:nth-of-type(2)";
	let anim12 = "animated slideInLeft";
	$(element12)
		.addClass('invisible');

	$(element12)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element12)
					.removeClass('invisible')
					.addClass(anim12)
					.one(animationEnd, function () {
						$(element12)
							.removeClass(anim12);
					});
			} else if (direction === 'up') {
				$(element12)
					.removeClass(anim12)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});


	// Animacion Servicios Import
	let element13 = "#servicios article:nth-of-type(3)";
	let anim13 = "animated slideInRight";
	$(element13)
		.addClass('invisible');

	$(element13)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element13)
					.removeClass('invisible')
					.addClass(anim13)
					.one(animationEnd, function () {
						$(element13)
							.removeClass(anim13);
					});
			} else if (direction === 'up') {
				$(element13)
					.removeClass(anim13)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});


	// Animacion Servicios technician
	let element14 = "#servicios article:nth-of-type(4)";
	let anim14 = "animated slideInLeft";
	$(element14)
		.addClass('invisible');

	$(element14)
		.waypoint(function (direction) {
			if (direction === 'down') {
				$(element14)
					.removeClass('invisible')
					.addClass(anim14)
					.one(animationEnd, function () {
						$(element14)
							.removeClass(anim14);
					});
			} else if (direction === 'up') {
				$(element14)
					.removeClass(anim14)
					.addClass('invisible');
			}
		}, {
			offset: '80%'
		});

	$(".percircle")
		.percircle({
			progressBarColor: "#01B27A"
		});
});